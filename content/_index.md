## Sekai 🌐 🗺

Sekai is the [kanji][1] for 世界, meaning "the world".  That's a great
word because of the scale it designates.

[1]: https://en.wikipedia.org/wiki/Kanji
