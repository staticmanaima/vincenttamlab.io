---
title: "About me"
subtitle: "Brief intro and resource list"
date: 2018-11-20T15:10:10+01:00
---

A math student and a GNU/Linux user, built @staticmanlab on [GitLab][gl] and
[GitHub][gh].

### Useful links

1. [Google Advanced Search Form - Google Guide][search]
2. [My little online math + Markdown editor with instant preview][my_math_editor]
2. [MATLAB--Python--Julia cheatsheet --- Cheatsheets by QuantEcon documentation][matlab_julia]
3. [R vs Julia cheatsheet - datascience-enthusiast.com][r_julia]
4. [Online Cangjie Input][canjie]
5. [Online compilers and terminals on Tutorials Point][ide]
6. [Solution to some classical math books][math_sol]
7. [$\LaTeX$ cheatsheet (in French)][latex_am_fr]

### My online graphs

1. [Trigonometric approximation of sawtooth wave][graph_sawtooth]
2. [Simple sine functions for limits of integrals][sine_leb]

[gl]: https://gitlab.com/staticmanlab
[gh]: https://github.com/staticmanlab
[search]: http://www.googleguide.com/sharpening_queries.html
[my_math_editor]: https://vincenttam.gitlab.io/math-live-preview/
[matlab_julia]: https://cheatsheets.quantecon.org/
[r_julia]: https://datascience-enthusiast.com/R/R_Julia_cheat_sheet.html
[canjie]: http://www.cangjieinput.com/
[ide]: https://www.tutorialspoint.com/codingground.htm
[math_sol]: http://homepage.ntu.edu.tw/~d04221001/Notes/Problems%20and%20Solutions%20Section/Problems%20and%20Solutions.html
[latex_am_fr]: http://www.edu.upmc.fr/c2i/ressources/latex/aide-memoire.pdf
[graph_sawtooth]: https://www.desmos.com/calculator/oa2fn5gdc6
[sine_leb]: https://www.desmos.com/calculator/hkvzpvcbhh
