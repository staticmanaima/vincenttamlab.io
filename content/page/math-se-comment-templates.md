---
title: "Math.SE Comment Templates"
date: 2018-12-11T12:38:28+01:00
type: page
draft: false
---

Here's a list of common comments for Math.SE newbies.

I've posted them on [WordPress.com][wp], but I often got typos after updating
that page.  *Without* version control, it's hard to find out the cause of such
error.

### Questions lacking context

<div><textarea readonly rows="6" cols="80">
[Welcome to Math.SE!](//math.meta.stackexchange.com/a/11168/290189) Please read [this post](//math.meta.stackexchange.com/a/9960/290189) and the others there for information on writing a good question for this site. In particular, people will be more willing to help if you [edit] your question to include some motivation, and an explanation of your own attempts.
</textarea></div>

{{< copyBtn >}}

### Questions not written in MathJax

<div><textarea readonly rows="6" cols="80">
[Welcome to Math.SE!](//math.meta.stackexchange.com/a/4928/290189) Please use MathJax.  For some basic information about writing math at this site see e.g. [basic help on MathJax notation](/help/notation), [MathJax tutorial and quick reference](//math.meta.stackexchange.com/q/5020/290189), [main meta site math tutorial](//meta.stackexchange.com/a/70559/259305) and [equation editing how-to](//math.meta.stackexchange.com/q/1773/290189).
</textarea></div>

{{< copyBtn >}}

### Newbies' combo

<div><textarea readonly rows="6" cols="80">
[Welcome to Math.SE!](//math.meta.stackexchange.com/a/11168/290189) [Please use MathJax.](//math.meta.stackexchange.com/a/4928/290189)  For some basic information about writing math at this site, see [MathJax tutorial and quick reference](//math.meta.stackexchange.com/q/5020/290189) and [equation editing how-to](//math.meta.stackexchange.com/q/1773/290189). Please read [this post](//math.meta.stackexchange.com/a/9960/290189) for writing a good question.
</textarea></div>

{{< copyBtn >}}

### Newbies' pic-question

To avoid excedding the upper limit of 500&nbsp;characters, I've copied
and pasted *several* comments and combined them into one.

<div><textarea readonly rows="6" cols="80">
[Welcome to Math.SE!](//math.meta.stackexchange.com/a/4928/290189) Please use MathJax and see [MathJax tutorial](//math.meta.stackexchange.com/q/5020/290189). Please [don't use pictures for critical portions](//math.meta.stackexchange.com/a/20529/290189) of your post. Pictures may not be legible, cannot be searched and are not viewable to those who use screen readers. Please read [this post](//math.meta.stackexchange.com/a/9960/290189) for information on writing a good question. People will be more willing to help if you [edit] your question to include an explanation of your own attempts.
</textarea></div>

{{< copyBtn >}}

### Image posts

<div><textarea readonly rows="6" cols="80">
Please do [not use pictures for critical portions](//math.meta.stackexchange.com/a/20529/290189) of your post. Pictures may not be legible, cannot be searched and are not viewable to some, such as those who use screen readers.
</textarea></div>

{{< copyBtn >}}

### Text images / Scanned pages from books

<div><textarea readonly rows="6" cols="80">
Please do [not use pictures for critical portions](//math.meta.stackexchange.com/a/20529/290189) of your post. Pictures may not be legible, cannot be searched and are not viewable to some, such as those who use screen readers. [Scanned pages from books are discouraged on SE network](//meta.stackexchange.com/q/155600/259305). Questions should contain sufficient context so that it is [answerable with the text alone](//math.meta.stackexchange.com/a/1807/290189).
</textarea></div>

{{< copyBtn >}}

### Displaystyle in question title

<div><textarea readonly rows="6" cols="80">
I have changed the formatting of the title so as to [make it take up less vertical space](//math.meta.stackexchange.com/a/9686/290189) -- this is a policy to ensure that the scarce space on the main page is distributed evenly over the questions.  See [here](//math.meta.stackexchange.com/a/9730/290189) for more information. Please take this into consideration for future questions. Thanks in advance.
</textarea></div>

{{< copyBtn >}}

### One question per post

<div><textarea readonly rows="6" cols="80">
Please, post only [one question in one post](//math.meta.stackexchange.com/a/7132/290189). Posting several questions in the same post is discouraged and such questions may be put on hold, see [meta](//math.meta.stackexchange.com/q/6464/290189).
</textarea></div>

{{< copyBtn >}}

### Fractions in indices and exponents

<div><textarea readonly rows="6" cols="80">
Please [don't use `\frac` in exponents or limits of integrals](//math.meta.stackexchange.com/a/5057/290189). It looks bad and confusing, and it rarely appears in professional mathematics typesetting.
</textarea></div>

{{< copyBtn >}}

### Accept answers

<div><textarea readonly rows="6" cols="80">
After you ask a question here, if you get an acceptable answer, please [consider "accept" the answer](//math.meta.stackexchange.com/a/4945/290189) by clicking the check mark $\checkmark$ next to it. This scores points for you and for the person who answered your question. You can find out more about accepting answers here: [How do I accept an answer?](//meta.math.stackexchange.com/questions/3286/), [Why should we accept answers?](//meta.math.stackexchange.com/questions/3399/), [What should I do if someone answers my question?](//math.stackexchange.com/help/someone-answers).
</textarea></div>

{{< copyBtn >}}

### Notify question writer for a $\LaTeX$ edit

<div><textarea readonly rows="6" cols="80">
[Welcome to Math.SE!](//math.meta.stackexchange.com/a/8588/290189) I have tried to improve the readability of your question by improving the [$\rm \LaTeX$](//math.meta.stackexchange.com/q/5020/290189) code. It is possible that I unintentionally changed the meaning of your question.  Please proofread the question to ensure this has not happened.
</textarea></div>

{{< copyBtn >}}

### Wrong tags

Some commonly misused tags.

#### Probability Theory

<div><textarea readonly rows="6" cols="80">
[Probability theory](//en.wikipedia.org/wiki/Probability_theory) is [about the measure-theoretic foundations](//math.meta.stackexchange.com/a/8587/290189) of stochastics.  The tag ([tag:probability-theory]) should be used for questions concerning this subject, not for questions about calculating a specific probability.  Use ([tag:probability]) instead, see also [meta](//math.meta.stackexchange.com/q/1686/290189).
</textarea></div>

{{< copyBtn >}}

#### Functional Analysis

<div><textarea readonly rows="6" cols="80">
The tag ([tag:functional-analysis]) is [intended for questions about infinite dimensional vector spaces](//math.meta.stackexchange.com/a/8587/290189), there is a separate tag for ([tag:functional-equations]); see the [tag-wiki](https://math.stackexchange.com/tags/functional-analysis/info) and the tag-excerpt. (The tag-excerpt is also shown when you are adding a tag to a question.)
</textarea></div>

{{< copyBtn >}}

#### Algebraic Geometry

<div><textarea readonly rows="6" cols="80">
The tag ([tag:algebraic-geometry]) is intended for questions in a [branch of mathematics called algebraic geometry](//math.meta.stackexchange.com/a/8587/290189) (see the [tag-wiki](https://math.stackexchange.com/tags/algebraic-geometry/info).) The tags ([tag:algebra-precalculus]) and/or ([tag:geometry]) should be used for basic problems that involve both algebra and geometry.
</textarea></div>

{{< copyBtn >}}

### My SQL script for searching old comments

You may execute [my comment search by user and keyword][cmt_query] by clicking
the link.

[wp]: //blogueun.wordpress.com/some-math-se-templates/
[cmt_query]: //data.stackexchange.com/stackoverflow/query/850812/comment-finder
