---
title: Git URL Shortener Exception
subtitle: A URL lengthened by git.io
type: post
categories:
- technical support
tags:
- GitHub
date: 2018-12-19T18:10:04+01:00
bigimg: [{src: "b4.png", desc: "GitHub URL shortener"}]
---

Due to limitations on the number of characters imposed by the site owner, URL
shorteners are useful for shortening comments containing URL(s).  Nonetheless,
while commenting on [my recent PR for Introduction][pr], I found that
[GitHub's URL shortener][git.io] had _lengthened_ the URL for [GitHub]'s home
page.

{{< beautifulfigure src="after.png" title="Output of \"https://github.com\" by GitHub's URL Shortener" caption="A counterexample of GitHub's URL Shortener" alt="github home page short url" >}}

Compare these two URLs.

    https://github.com
    https://git.io/xE9D

[pr]: https://github.com/vickylai/hugo-theme-introduction/pull/119
[GitHub]: https://github.com/
[git.io]: https://git.io/
