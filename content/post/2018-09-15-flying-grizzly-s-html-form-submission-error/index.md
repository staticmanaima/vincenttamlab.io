---
title: "Flying Grizzly's HTML Form Submission Error"
subtitle: "Filling in hidden missing fields"
date: 2018-09-15T23:09:06+02:00
type: post
categories:
- technical support
tags:
- HTML
- form
draft: false
---

### Problem

I would like to submit [Flying Grizzly's form][form].  I filled in _every_
blanks and then I clicked the "submit" button below.  I was greeted with a
`MISSING_REQUIRED_FIELDS` error within a second.

{{< gallery >}}
  {{< beautifulfigure link="comment.png" thumb="-s" title="HTML form" caption="every entry filled in" alt="comment form" >}}
  {{< beautifulfigure src="response-s.png" title="Staticman v2 server response" caption="500 internal server error" alt="html server error" >}}
{{< /gallery >}}

{{< highlight json >}}
{
    "success":false,
    "data":["replying_to"],
    "rawError":{
        "_smErrorCode":"MISSING_REQUIRED_FIELDS",
        "data":["replying_to"]
    },
    "errorCode":"MISSING_REQUIRED_FIELDS"
}
{{< /highlight >}}

### Discussion

The error code suggested that the form fields sent should be inspected.

1. Open the Web Developer Tools of your web browser. (press `<F12>`)
2. Select **Network** tab.
3. Click on the entry "500", which represents an internal server error.
4. In the **Params** side pane, observe that `fields[replying_to]` has _empty_
value.  This is the source of error.

{{< beautifulfigure src="response-s.png" title="Missing hidden field" caption="fields[replying_to] empty" alt="missing hidden field" >}}

### Solution

Right click on _any_ element of the HTML form and select **Inspect element**.
Under

{{< highlight html >}}
<input id="comment_parent" name="fields[replying_to]" value="" type="hidden">
{{< /highlight >}}

{{< gallery >}}
  {{< beautifulfigure src="inject1.png" title="Search \"replying_to\"" caption="fields[replying_to] empty" alt="inspect element for hidden field" >}}
  {{< beautifulfigure src="inject2.png" title="Value \"abc\" injected" caption="fields[replying_to] nonempty" alt="inject value for hidden field" >}}
{{< /gallery >}}

inject _whatever_ you like, then click "submit" again.

### Result

The error disappeared.  After submission, I was redirected to the page.  The
HTTP status code "302" represented a redirection.

{{< gallery >}}
  {{< beautifulfigure link="result1.png" thumb="-s" title="POST request sent" caption="got 304 status code" alt="Staticman v2 response" >}}
  {{< beautifulfigure link="result2.png" thumb="-s" title="POST request sent" caption="response empty" alt="redirected back to blog" >}}
{{< /gallery >}}

[form]: https://www.flyinggrizzly.net/2017/12/setting-up-staticman-server/
