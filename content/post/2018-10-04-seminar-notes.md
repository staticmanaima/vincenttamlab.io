---
title: 2018-10-04 Seminar Notes
date: 2018-10-04T02:16:44+02:00
type: post
categories:
- math
tags:
- seminar
- probability
- statistics
toc: true
draft: false
markup: mmark
---

I jotted down only a few keywords that might be reusable.  I _didn't_ understand
any of the talks.

### Functional Data Analysis

- Goal: predict equipment temperature
- Tools: Fourier coefficients (trigo ones), followed by discretisation,
min-error estimation, cross-validation 10-folds, $R^2$ adjusted ?, MAE, MSPE
- Comparison with non-functional data

### Tolérancement

- Thème : Traiter les incertitudes sur les dimensions des pièces de l'avion
- Objectif :
  + établir une modélisation mathématiques
  + construire un virtual twin de l'avion
- Outils :
  + Modèle de variabilité
  + Modèle d'assemblage $\text{airbus}: Y = \sum_{i = 1}^n a_iX_i?$
  + Notion de risque ... calculs des coefficients de convolution

### SVM

- Multiclass vs structual, hidden Markov model
- Plan for this year:
  + apply structual SVM for real SVM
  + apply structual SVM for deep neural network

### Auxiliary information

- auxiliary function given in one partition
- auxiliary function given in mutiple partitions
- bootstrap
- law of iterated logarithms
- Kullback--Leibler distance
- convergence: Donsker class, var, covar
- ranking ration method: convergence to Gaussian process, entropy conditions,
Telegrandś inequality
  + weak convergence: KMT, Berthet-Maison
  + strong convergence: ?
    + consequences: Berry-Essen bound, bias & variance estimation of ranking
    ration method

### Euler scheme SDE

I could only write "Toeplitz tape operator".

### Transport optimal

Il n'y avait pas de transparent.  J'étais d'autant loin du tableau que je ne
voyais pas bien tout ce que le doctorant écrivait, comme

> $\mu, \nu \in \mathcal{P}(\R^d)$  
> $T: \R^d \to \R^d$ tq $T \mu = \nu$
> ...  
> Thm (Branly)

### CORE-cluster

Il s'agit d'un algo de classification qui cherche à minimaliser les distances
entre les CORE-clusters.  On approxime l'arbre optimal par l'algo de Kruskal.
On prend les chemins plus lourds.  Une application biologique concerne les
levures.

### Limite d'échelles sur la marche aléatoire

Marche aléatoire sur $\Z^{K+1}$, constrainte de forme $|Z^{(i+1)}-Z^{(i)}| = 1$
pour tout $i = 0, \dots, K$.  On impose la loi uniforme sur le voisinage de
chacun, et on considère la chaîne de prisonniers.

> Thm (Boissard, Cohen, Norris, ???, 2015)
>
> $$
> \Big(\frac{1}{\sqrt{n}} X\_{n,k} (t)\Big)\_{t\ge0} \xrightarrow{\mathcal{D}} (B_t)\_{t\ge0},
> $$
>
> où $(B_t)\_{t\ge0}$ est un mouvement brownien de variance
> $\frac{K}{K+2}$.
>
> <i class="fas fa-exclamation-triangle fa-lg fa-pull-left"></i>
> The above equation is probably _wrong_.

« champs des vecteurs » $A_k$ sur $G_k$.  $A_k = B_k + \nabla f_k$

$\nabla f_k$ est le gradient du potentiel $V_k$.

$$
Z_{n,K}^{(1)} = Z_{n,K}^{(0)} + \sum_{i = 1}^n B_K (Y_K(i),Y_K(i+1)) +
\sum_{i = 1}^n \nabla f_k (Y_K(i),Y_K(i))
$$

- cas $n = K$: $X_{n,K}$ est une urne d'Ehrenfrest (déguisée)
- cas $n \simeq K$: ???
- cas $n >> K$: $c_{n,K} = \sqrt{K}$, sinon on écrase le processus  
Nouveau « chaos » introduit par l'accelération de $n$
- cas $n << K$:

    > Thm
    >
    > $$
    > \left(\frac{X\_{n,K}(t)-X\_{n,K}(0)}{\sqrt{n}}\right)\_{t\ge0}
    > \xrightarrow[n/K \to 0]{\mathcal{D}} (B\_t)\_{t\ge0}
    > $$

### Défaillance

- fiabiliabité ⟹ la théorie des extrêmes
- loi conjointe: on n'a pas d'accès ⟹ apprentissage statistique
- deux approaches principales :
  + block max
  + peak-over thershold
- différentier les lois
- utiliser les coupules
  + cas $d = 2$: Gauss, Gumbel, Franck, mélange
- définition du R-vine

### Relational databases and bayesian networks

Max Halford's slides on GitHub are _gone_.
