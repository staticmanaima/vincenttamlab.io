---
title: "Xubuntu Dualboot on Fujitsu LH532"
subtitle: "Convert Ubuntu to Legacy Mode with Boot-Repair"
date: 2018-06-28T21:50:45+02:00
type: post
categories:
- technical support
tags:
- Linux
- Xubuntu
- BIOS
- UEFI
- dual boot
- Fujitsu Lifebook
- LH532
---

You may skip to the [acutal setup](#blow-up-and-rebuild).

### Background

After using Win 7 & 10 for six years on my Fujitsu LH532 <i class="fa
fa-laptop" aria-hidden></i>, Windows 10 often falls into blue screen.
Sometimes, it *won't* even start up.

### Give Xubuntu a try

Hoping to give a second life to my old <i class="fa fa-laptop"
aria-hidden></i>, I've decided to try this popular lightweight
distro.

#### Initial partition setup

I followed the instructions from [Danatela's question][1], including
the optional setup of separate `/var` and `/tmp` partition.

Everything ran smooth until **"`grub-efi-amd64-signed` package failed to
install"**, so the installed Xubuntu **won't boot**.

| `/dev/sdaX` | mount point |
|-------------|-------------|
|         `6` | `/`         |
|         `7` | `/home`     |
|         `8` | `/boot`     |
|         `9` | `/var`      |
|        `10` | `/tmp`      |

#### Understanding BIOS Legacy vs UEFI

The installation process was far from easy: I had to install it in
BIOS Legacy the mode, but my 64-bit live USB booted in UEFI mode from
the BIOS (Version 1.10).  In the live session, I double-checked the
existence of the folder `/sys/firmware/efi/`; in Win 10, I ran
"msinfo32" to confirm that it's installed in BIOS Legacy mode.

I *can't* find a way to boot a 64-bit live USB in BIOS Legacy mode.  I
booted the live session by pressing `<F12>` after switching on, thus
booting from BIOS.  There's only one relevant entry for USB stick, so
it *certainly* booted from BIOS, but into *UEFI* mode due to the
presence of an EFI partition made by [Rufus][2].  I've also tried
[UNetBootIn][3] and [Linux's disk image writer][4] with 64-bit OS ISO
file.  All of these methods lead to a live session in *UEFI* mode.

After some searching, I've understood that *the mode of the live
session (BIOS Legacy/UEFI) determines that of the installed OS*.

#### First boot-repair attempt

At this stage, I was tempted to convert Xubutnu to Legacy mode.  The
[Community Help][5] suggested using "Advanced options" in
[Boot-Repair][6].  However, I was faced with another error: this
software told me to "[close all package managers][7]" and try again
later.

I wasted hours finding the possible causes, but that's fruitless.
Someone says **[Boot-Repair][6] *doesn't* support separate `/var`
partition**.

It could still boot into Win 10, so I *didn't* dare to overwrite the
MBR.  (An OS will try to overwrite the MBR so that it's bootable.
Problem arises when you try to install another OS.  As a result, boot
menu setup is needed, either automatically or manually.)

Here's the partition table generated by [Boot-Info][6].

    Drive: sda _____________________________________________________________________
    Disk /dev/sda: 465.8 GiB, 500107862016 bytes, 976773168 sectors
    Units: sectors of 1 * 512 = 512 bytes
    Sector size (logical/physical): 512 bytes / 512 bytes
    I/O size (minimum/optimal): 512 bytes / 512 bytes
    Disklabel type: dos

    Partition  Boot  Start Sector    End Sector  # of Sectors  Id System

    /dev/sda1               2,048    33,558,527    33,556,480  27 Hidden NTFS (Recovery Environment)
    /dev/sda2    *     33,558,528    33,968,127       409,600   7 NTFS / exFAT / HPFS
    /dev/sda3          33,968,128   366,962,687   332,994,560   7 NTFS / exFAT / HPFS
    /dev/sda4         366,964,734   475,252,735   108,288,002   5 Extended
    /dev/sda5         366,964,736   383,963,135    16,998,400  82 Linux swap / Solaris
    /dev/sda6         383,965,184   433,963,007    49,997,824  83 Linux
    /dev/sda7         433,965,056   453,963,775    19,998,720  83 Linux
    /dev/sda8         453,965,824   454,940,671       974,848  83 Linux
    /dev/sda9         454,942,720   465,096,703    10,153,984  83 Linux
    /dev/sda10        465,098,752   475,252,735    10,153,984  83 Linux

I also found the line "*BIOS is EFI-compatible, and is setup in
EFI-mode for this live-session.*"

#### Manual GRUB fix attempt

I also tried the commands on [How to Ubuntu][8], with an additional
command.

    sudo mount --bind /var /mnt/var

However, when I `update-grub`, it complained the *lack of
[EFI partition][9]*.

#### Blow-up and rebuild

##### Ubiquity installer

After searching on the matter for another several hours, I decided to
*redo the installation* according to
[IT'zGeek's installation guide][10] to verify
[**the above claim about `/var`**](#first-boot-repair-attempt).

I *don't* remember the exact partition size type, but their
approximate size.

| `/dev/sdaX` | mount point | Size   |
|-------------|-------------| ------ |
|         `5` | (swap)      | 8.2GB  |
|         `6` | `/boot`     | 500MB  |
|         `7` | `/`         | 30GB   |

<i class="fa fa-exclamation-circle"></i> **Use the
partition scheme for BIOS Legacy even though the live session is
running in UEFI mode.**  This will introduce errors which will be
fixed by [Boot-Repair][6] later.

As expected, the installer *collapsed* in the middle of the
installation, when it tried to install `grub-efi-amd64-signed`.  I got
into the *same* error dialog box.

##### Second Boot-Repair attempt

Noonetheless, *[Boot-Repair][6]'s "Recommended repair" worked this
time*: it suggested three commands (that I've forgotten) to be issued
in a dialog box.  I followed the suggested steps.  Despite the error
message displayed in the program, I finally **managed to have a GRUB 2
menu** when I restarted the machine.

> An error occurred during the repair.

> Please write on a paper the following URL:
> http:// paste.ubuntu.com/p/csGtkmSs6d/

> In case you still experience boot problem, indicate this URL to:
boot.repair@gmail.com 

> You can now reboot your computer.

> The boot files of [Ubuntu 18.04 LTS] are far from the start of the disk. Your BIOS may not detect them. You may want to retry after creating a /boot partition (EXT4, >200MB, start of the disk). This can be performed via tools such as gParted. Then select this partition via the [Separate /boot partition:] option of [Boot Repair]. (https://help.ubuntu.com/community/BootPartition)

<i class="fa fa-info-circle" aria-hidden></i> In *this* live session, the roles
of `/dev/sda` and `/dev/sdb` were *inverted*.  Since the link to
[Ubuntu Pastebin][11] is *temperarory*, I *intentionally* added a whitespace in
the middle so that Google Bot *won't* complain about the dead link in a year or
later.

##### Last steps

The GRUB2 menu *didn't* show Win 10.  This is easily solved by
`os-prober` and `update-grub`, both run as `sudo`.  Then, I continued
the `apt` installation by running `update` and `upgrade` and restarted
the machine. I found *three* Win 10 entries in the boot menu.

    *Ubuntu
     Advanced options for Ubuntu
     Windows Recovery Environment (on /dev/sda1)
     Windows 10 (on /dev/sda2)
     Windows 10 (on /dev/sda3)

Fabrizio C provided [an excellent answer][12] on how to remove any one
of them.  The instance `${OS}` in the `for` loop represents each line
of the output of `sudo os-prober`.

    $ sudo os-prober
    /dev/sda1:Windows Recovery Environment:Windows:chain
    /dev/sda2:Windows 10:Windows1:chain
    /dev/sda3:Windows 10:Windows2:chain

For example, `${LABEL}` takes value in the third column with `^`
replaced by whitespace.

You may have a look at [my `/etc/grub.d/30_os-prober`][13] (lines
158--166) for details.

[1]: https://askubuntu.com/q/343268/259048
[2]: https://rufus.akeo.ie/
[3]: https://unetbootin.github.io/
[4]: http://www.debugpoint.com/2016/07/how-to-write-iso-image-to-usb-drive-in-ubuntu/
[5]: https://help.ubuntu.com/community/UEFI#Converting_Ubuntu_into_Legacy_mode
[6]: https://help.ubuntu.com/community/Boot-Repair
[7]: https://askubuntu.com/q/758886/259048
[8]: https://howtoubuntu.org/how-to-repair-restore-reinstall-grub-2-with-a-ubuntu-live-cd
[9]: https://en.wikipedia.org/wiki/EFI_system_partition
[10]: https://www.itzgeek.com/how-tos/linux/ubuntu-how-tos/how-to-install-ubuntu-18-04-lts-bionic-beaver-on-uefi-and-legacy-bios-system.html
[11]: https://paste.ubuntu.com/
[12]: https://superuser.com/a/473081
[13]: https://gitlab.com/snippets/1728767#L158-L166
