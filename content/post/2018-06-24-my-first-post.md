---
title: "My First Post"
date: 2018-06-24T02:05:00+02:00
type: post
categories:
- blogging
tags:
- Hugo
- Octopress
- WordPress
---

### Introduction

Hello World!  This is my new GitLab page powered by Hugo.  I'm setting
up this blog to practise my math, foreign languages and IT skills.

### Why Hugo?

My original goal is to set up a personal blog for posting math.

I used to work with Octopress, but resolving the dependencies and
other technical issues  had actually took *much* more time and effort
than expected.  As a result, I digressed a lot from my studies to look
into those problems.

I tried using WordPress.com, but typing `$latex $` at the beginning of
*each* math expression is so tedious that it breaks the workflow:
careful proofread is needed to spot out typos.  In addition, the lack
of support for display style equation complicates the code.  Finding a
*reliable* free host for the full version WordPress is, in my opinion,
a mission impossible.

Contrary to Octopress, Hugo is self-contained.  I can write math
offline from a text editor and post it afterward.
