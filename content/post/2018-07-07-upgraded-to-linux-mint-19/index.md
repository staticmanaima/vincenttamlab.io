---
title: "Upgraded to Linux Mint 19"
subtitle: "Thanks to mintupgrade"
date: 2018-07-07T21:14:02+02:00
type: post
categories:
- technical support
tags:
- Linux
- Linux Mint
- upgrade
---

### Rationale

My [Linux Mint 18.3][1] installed on Clevo N240GU <i class="fa
fa-laptop" aria-hidden></i> was a long-term support (LTS) version.
Although the [official page][2] *didn't* recommend an upgrade for its
own sake, I still performed it as a result of my conversation with a
technician in a computer store.  He thought that the display problem
in the current version would be *fixed* in this upgraded version.

{{< beautifulfigure src="20180706_213156_HDR.jpg" alt="Linux Mint 18.3" >}}

The old version of Linux Mint *couldn't* detect the onboard Intel
graphics card.  Therefore, `sensors` reported a higher CPU temperature
and the display resolution *couldn't* be correctly configured.

### Process

[Easy Linux tips project][3] recommends a clean upgrade, but I *don't*
wish to change the partition scheme, which was set up the technicians
in the computer store last Friday.  Consequently, I followed the
[community guide][4].

During the upgrade, gconf2 gave me an error message.  I *don't* recall what
it was exactly.  Below that message, a list of packages to be updated
and installed were shown.  About 600 packages were kept from upgrade,
including [Firefox][5].

### Result

On the whole, the upgrade was successful.  After a reboot, I could
enter into the new OS.  The display has become much more clearer.
However, the fonts are too *small*.

{{< beautifulfigure src="20180706_222740.jpg" alt="Upgraded system" >}}

### Finishing touch

All the recommended settings in
[*10 things to do first in Linux Mint 19 MATE*][6] were implemented,
even the [steps for SSD optimisation][7].  I checked the list of fatal
mistakes, and I enabled [UFW][8].  However, I was so *lazy* that I
*didn't* tweak [LibreOffice][9].

[1]: https://linuxmint.com/edition.php?id=248
[2]: https://blog.linuxmint.com/?p=3615
[3]: https://sites.google.com/site/easylinuxtipsproject/upgrademint#TOC-From-18.x-to-19:-before-you-start:-is-it-wise-to-upgrade-at-all-yet-
[4]: https://community.linuxmint.com/tutorial/view/2416
[5]: https://www.mozilla.org/en-US/firefox/new/?icn=tabz
[6]: https://sites.google.com/site/easylinuxtipsproject/mint-mate-first
[7]: https://sites.google.com/site/easylinuxtipsproject/ssd
[8]: https://launchpad.net/ufw
[9]: https://www.libreoffice.org/
