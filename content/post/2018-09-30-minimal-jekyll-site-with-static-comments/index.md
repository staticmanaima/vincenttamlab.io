---
title: Minimal Jekyll Site with Static Comments
subtitle: Setup Staticman v3 and Jekyll on GitHub Pages
date: 2018-09-30T23:59:14+02:00
type: post
categories:
- blogging
tags:
- Jekyll
- GitHub Pages
- Staticman
draft: false
---

### Introduction

This is the [GitHub Pages][gp] version to
[my GitLab Pages with Staticman tutorial][pp].

I _didn't_ plan to test whether [Staticman v3][smv3] work on [GitHub] since it's
_proprietary_.  However, from Staticman issues [#222][i222] and [#227][i227],
we know that the official server _doesn't_ respond to

    GET /v2/connnect/<USERNAME>/<REPONAME>

To help others, I self-advertised [my own Staticman API instance][sm3] and the
migration to [GitLab pages][gp2].  _Unfortunately_, nobody had managed to
create a GitHub repo running on my API instance.  To convince others that it's
also _working_ on [GitHub], I decided to create a minimal GitHub repo.

### Why Jekyll?

[GitHub] has built-in [Jekyll] support.  As long as the file infrastructure is
there, [GitHub] will automatically attempt to build the site from the code in
the repo using the static site generator [Jekyll].  This is quite convenient.

### Setup

You may view [the GitHub repo for my demo site][src].  I'm too lazy to re-type
the details. 

{{< beautifulfigure src="staticmanlab-invitation.png" width="800" title="Allow @staticmanlab to accept invitation" caption="Screenshot taken on 30th Sep, 2018" alt="Programmatically accept invitation on Staticman" >}}

{{< beautifulfigure src="staticman-ok.png" title="A successful comment posted on demo site" caption="Screenshot taken on 30th Sep, 2018" alt="comment posted successfully" >}}

### Other GitHub Pages served by Staticman v3

1. A fork of my demo Jekyll site at ~cambragol.github.io/TestStaticmanLab/~
2. [Ultima IV Trinity][trinity]
3. [Cohere City's pledge][cohere-city]
4. [Static Chuck!][chuck] (Another clone of my demo GitHub repo)
5. [Des voyages, une aventure][sdessus] (in French)

<i class="fas fa-info-circle fa-lg fa-pull-left" aria-hidden></i>
Upgrading to v3 *doesn't* mean dropping backward compatibility for v2.  (at
least, in the near future)  Here's a (non-exhausive) list of sites running on a
custom API server.

1. [Personal Website Of Morteza Asadi][asadi] (in Iranian)
2. [Ricardo Adão's blog][ricardonadao]
3. [Thoughts @skryl.org][skryl]

[gp]: https://pages.github.com/
[gp2]: https://docs.gitlab.com/ee/user/project/pages/
[pp]: /post/2018-09-16-staticman-powered-gitlab-pages/1/
[smv3]: https://dev.staticman.net
[GitHub]: https://github.com
[i222]: https://github.com/eduardoboucas/staticman/issues/222
[i227]: https://github.com/eduardoboucas/staticman/issues/227
[sm3]: https://staticman3.herokuapp.com/
[src]: https://git.io/fxf18
[Jekyll]: https://jekyllrb.com
[trinity]: https://cambragol.github.io/ultima-IV-trinity
[chuck]: http://www.chuckmasterson.com/staticchuck-test/
[cohere-city]: https://cohere-city.github.io/pledge/
[asadi]: http://asadiweb.ir
[sdessus]: https://sdessus.github.io
[ricardonadao]: https://ricardonadao.github.io
[skryl]: https://blog.skryl.org/
