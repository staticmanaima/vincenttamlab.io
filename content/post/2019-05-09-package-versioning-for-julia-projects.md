---
title: "Package Versioning for Julia Projects"
subtitle: "Resolved GLM installation error"
date: 2019-05-09T13:32:11+02:00
categories:
- technical support
tags:
- Julia
- GLM
draft: false
toc: true
---

> **Update**: Thanks to [JuliaRegistries/General#647][pr647], the version cap
> of the package [Distributions] has now been removed, so that one can directly
> install the package [GLM] along with [Distributions] v0.19.2.

# Background

I'm reading the book [*Statistics with Julia*][book], which makes use of [GLM].

# Problem

I encountered the following error, which I posted in the issue
[GLM.jl#311][GLM#311].

```
(v1.1) pkg> add GLM
 Resolving package versions...
ERROR: Unsatisfiable requirements detected for package GLM [38e38edf]:
 GLM [38e38edf] log:
 ├─possible versions are: [0.2.0-0.2.5, 0.3.0-0.3.2, 0.4.0-0.4.8, 0.5.0-0.5.6, 0
.6.0-0.6.1, 0.7.0, 0.8.0-0.8.1, 0.9.0, 0.10.0-0.10.1, 0.11.0, 1.0.0-1.0.2, 1.1.0
-1.1.1] or uninstalled
 ├─restricted to versions * by an explicit requirement, leaving only versions [0
.2.0-0.2.5, 0.3.0-0.3.2, 0.4.0-0.4.8, 0.5.0-0.5.6, 0.6.0-0.6.1, 0.7.0, 0.8.0-0.8
.1, 0.9.0, 0.10.0-0.10.1, 0.11.0, 1.0.0-1.0.2, 1.1.0-1.1.1]
 ├─restricted by compatibility requirements with DataFrames [a93c6f00] to versio
ns: [0.4.0-0.4.8, 0.5.0-0.5.6, 0.6.0-0.6.1, 0.7.0, 0.8.0-0.8.1, 0.9.0, 0.10.0-0.
10.1, 0.11.0, 1.0.0-1.0.2, 1.1.0-1.1.1] or uninstalled, leaving only versions: [
0.4.0-0.4.8, 0.5.0-0.5.6, 0.6.0-0.6.1, 0.7.0, 0.8.0-0.8.1, 0.9.0, 0.10.0-0.10.1,
 0.11.0, 1.0.0-1.0.2, 1.1.0-1.1.1]
 │ └─DataFrames [a93c6f00] log:
 │   ├─possible versions are: [0.1.0, 0.2.0-0.2.5, 0.3.0-0.3.16, 0.4.0-0.4.3, 0.
5.0-0.5.12, 0.6.0-0.6.11, 0.7.0-0.7.8, 0.8.0-0.8.5, 0.9.0-0.9.1, 0.10.0-0.10.1, 
0.11.0-0.11.7, 0.12.0, 0.13.0-0.13.1, 0.14.0-0.14.1, 0.15.0-0.15.2, 0.16.0, 0.17
.0-0.17.1, 0.18.0-0.18.2] or uninstalled
 │   └─restricted to versions 0.18.2 by an explicit requirement, leaving only ve
rsions 0.18.2
 ├─restricted by compatibility requirements with StatsBase [2913bbd2] to version
s: [0.11.0, 1.0.0-1.0.2, 1.1.0-1.1.1] or uninstalled, leaving only versions: [0.
11.0, 1.0.0-1.0.2, 1.1.0-1.1.1]
 │ └─StatsBase [2913bbd2] log:
 │   ├─possible versions are: [0.1.0, 0.2.0-0.2.1, 0.2.3-0.2.10, 0.3.0-0.3.13, 0
.4.0-0.4.4, 0.5.0-0.5.3, 0.6.0-0.6.16, 0.7.0-0.7.4, 0.8.0-0.8.3, 0.9.0, 0.10.0, 
0.11.0-0.11.1, 0.12.0, 0.13.0-0.13.1, 0.14.0-0.14.1, 0.15.0, 0.16.0-0.16.1, 0.17
.0, 0.18.0, 0.19.0-0.19.5, 0.20.0-0.20.1, 0.22.0, 0.23.0-0.23.1, 0.24.0, 0.25.0,
 0.26.0, 0.27.0, 0.28.0-0.28.1, 0.29.0, 0.30.0] or uninstalled
 │   └─restricted to versions 0.30.0 by an explicit requirement, leaving only ve
rsions 0.30.0
 └─restricted by compatibility requirements with Distributions [31c24e10] to ver
sions: uninstalled — no versions left
   └─Distributions [31c24e10] log:
     ├─possible versions are: [0.1.0-0.1.4, 0.2.0-0.2.13, 0.3.0, 0.6.4-0.6.7, 0.
7.0-0.7.6, 0.8.0-0.8.10, 0.9.0, 0.10.0-0.10.2, 0.11.0-0.11.1, 0.12.0-0.12.5, 0.1
3.0, 0.14.0-0.14.2, 0.15.0, 0.16.0-0.16.4, 0.17.0, 0.18.0, 0.19.1-0.19.2] or uni
nstalled
     └─restricted to versions 0.19.2 by an explicit requirement, leaving only ve
rsions 0.19.2
```

# Discussion

Although the packages [StatsBase] and [DataFrames] appear in the above error
message, the cause of this problem was actually the package [Distributions].

The system first checked the "compatibility requirements", then it showed the
matching version number or "uninstalled" on the right-hand side "leaving only
versions" of the same line.

I had installed [Distributions] through the `pkg`'s REPL, so I had the package
at version 0.19.2 only. From the answer in the [linked GitHub issue][GLM#311],
it's _incompatible_ with [GLM] for the moment.

I had mistaken the short package UUID "`31c24e10`" as the package release's Git
SHA1 hash.  To easily check the package's compatibility, I should have looked
for the section `[compat]` in the package's `project.toml`.

# Solution

Now, it's clear that I should install an older version of the package
[Distributions] for [GLM]'s installation.  Thanks to
[Stefan Karpinski's answer on Julia Discourse][soln], I've successfully solved
this incompatibility problem.

```julia
julia> # press `]` to activate the package REPL mode...
(v1.1) pkg> activate .
(StatsWithJuliaBook) pkg> add Distributions@0.18.0
(StatsWithJuliaBook) pkg> add GLM
```

`v1.1` refers to `~/.julia/environments/v1.1`; `StatsWithJuliaBook` is the
current working directory.

[book]: https://people.smp.uq.edu.au/YoniNazarathy/julia-stats/StatisticsWithJulia.pdf
[GLM]: https://github.com/JuliaStats/GLM.jl
[GLM#311]: https://github.com/JuliaStats/GLM.jl/issues/311
[StatsBase]: https://github.com/JuliaStats/StatsBase.jl
[DataFrames]: https://github.com/JuliaData/DataFrames.jl
[Distributions]: https://github.com/JuliaStats/Distributions.jl
[soln]: https://discourse.julialang.org/t/is-any-way-to-install-multiple-versions-of-same-package/20898/5?u=vincenttam
[pr647]: https://github.com/JuliaRegistries/General/pull/647
