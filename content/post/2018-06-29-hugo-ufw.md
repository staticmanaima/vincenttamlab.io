---
title: "Configure UFW for Hugo"
subtitle: "Simple Linux firewall settings for home computers"
type: post
categories:
- technical support
tags:
- Hugo
- Linux
- UFW
date: 2018-06-29T06:36:21+02:00
---

[Easy Linux tips project][1] recommends users to enable their
firewall.  However, this blocks all incoming traffic by default.  If
one wants to [test the site on mobile devices][2], one will need to
add a few rules to [UFW][3].

    $ sudo ufw allow ssh
    $ sudo ufw allow http
    $ sudo ufw allow https
    $ sudo ufw allow 80/tcp
    $ sudo ufw allow 1313/tcp comment "hugo server"

I learnt the last line from [竹内電設][4].

To extract the user-added rules, run [`ufw show added`][5].

    $ sudo ufw show added
    Added user rules (see 'ufw status' for running firewall):
    ufw allow 1313/tcp comment 'hugo server'
    ufw allow 22/tcp
    ufw allow 80/tcp
    ufw allow 443/tcp

[1]: https://sites.google.com/site/easylinuxtipsproject/security#TOC-Firewall
[2]: https://vincenttam.github.io/blog/2016/08/21/website-preview-in-mobile-devices/
[3]: https://launchpad.net/ufw
[4]: https://xn--v6q832hwdkvom.com/post/ufw/#%E3%83%AB%E3%83%BC%E3%83%AB%E3%81%AB%E3%82%B3%E3%83%A1%E3%83%B3%E3%83%88%E3%82%92%E8%BF%BD%E5%8A%A0%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF
[5]: https://askubuntu.com/a/533664/259048
