---
title: "Xubuntu Screen Enlargement"
date: 2019-04-21T02:49:09+02:00
categories:
- technical support
tags:
- Linux
draft: false
---

Tonight, the screen of Xubuntu suddenly enlarged and blurred.  Typing "xubuntu
screen display enlarged" on Google, I've learnt that the `<Alt>` key and the
mouse scroll wheel can enlarge/minify screen display from [Ubuntu Forums][1].

[1]: https://ubuntuforums.org/showthread.php?t=2349888
