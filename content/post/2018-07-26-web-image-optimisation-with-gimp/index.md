---
title: "Web Image Optimisation with GIMP"
subtitle: "My preferred parameters for web image compression"
date: 2018-07-26T10:52:48+02:00
type: post
categories:
- blogging
tags:
- GIMP
- image optimization
---

### Why image optimization?

To save loading <i class="far fa-clock" aria-hidden="true"></i>.

### My old way

[ImageMagick][1] provides a great
<span class="fa-stack">
  <i class="fa fa-square fa-stack-2x"></i>
  <i class="fa fa-terminal fa-stack-1x fa-inverse"></i>
</span>
CLI utility to `convert` and compress images
for any purposes.  However, to `-crop` images, I have to specify the numbers in
*pixels*.  One certainly works much more efficiently with a *GUI* tool.

### My new way

[GIMP][2] can do *anything* that [Adobe Photoshop][3] can do, even
[saving images for web][4].  In the linked page to this powerful GIMP plugin
tutorial, the link for the GNU/Linux binary file is *broken*.

#### Compile from source code (optional)

Unluckily, [`autogen.sh`][5] *can't* recognise the [Automake][6] 1.15 installed.
I tried following all answers in [a related <i class="fab fa-stack-overflow" aria-hidden="true"></i> question][7],
but they *didn't* solve the problem.

#### Installation on GNU/Linux

To download it for Debian-based distros, you may
[search for **gimp-plugin-registry**][8] in your favourite package manager
([Aptitude][9], [Synaptic][10], etc).

#### My preferred parameters <i class="fa fa-cog" aria-hidden="true"></i>

- Max dimension: 1200 px
- JPEG for photos <i class="fa fa-camera" aria-hidden="true"></i>
    + Quality: 0.8--0.95 (0.6 is OK for huge photos <i class="far fa-file-image" aria-hidden="true"></i>)
    + Smoothness: 0.10--0.15
    + <i class="fa fa-check"></i> Progressif
    + <i class="fas fa-times" aria-hidden="true"></i> EXIF data

- PNG for computer drawn raster graphics
    + _Transparent background_ if possible to discard background noise (with
    [<i class="fa fa-magic fa-flip-horizontal" aria-hidden="true"></i> Magical Wand][11] and
    [<i class="fas fa-cut fa-rotate-270" aria-hidden="true"></i> Intelligent Scissors][12]).
    + Use _black and white colors for black-and-white pictures_ <i class="fa
    fa-file-image-o" aria-hidden="true"></i>.

### Results

You may view this blog's
[<i class="fab fa-gitlab" aria-hidden="true"></i> @ c2f1299f][13] for details.

#### Photos <i class="fa fa-camera" aria-hidden="true"></i>

|   | <i class="far fa-file-image fa-2x" aria-hidden="true"></i> | ![compressed file icon][14] |
|---|---|---|
| ![Fujitsu LH532 CPU fan][15] | 735 KB | 271 KB |
| ![Fujitsu LH532 CPU fan exhaust][16] | 558 KB | 236 KB |
| ![Fujitsu LH532 CPU warning][17] | 881 KB | 312 KB |
| ![Fujitsu LH532 Xubuntu 18.04][18] | 912 KB | 86.4 KB |

<i class="fa fa-info-circle fa-lg" aria-hidden="true"></i> Icon from [oNline Web Fonts][19].

#### Computer drawn raster graphics
|   | <i class="far fa-file-image fa-2x" aria-hidden="true"></i> | ![compressed file icon][14] |
|---|---|---|
| ![rodéo sur cheval][20] | 21.1 KB | 1.6 KB |
| ![rodéo sur moto][21] | 56.4 KB | 3.71 KB |

<i class="fa fa-info-circle fa-lg" aria-hidden="true"></i> Icon from [oNline Web Fonts][19].

[1]: https://www.imagemagick.org/
[2]: https://www.gimp.org
[3]: https://www.adobe.com/Photoshop
[4]: http://templatetoaster.com/tutorials/gimp-save-for-web/
[5]: https://github.com/auris/gimp-save-for-web
[6]: https://www.gnu.org/software/automake
[7]: https://stackoverflow.com/q/21716385
[8]: https://www.diversidadyunpocodetodo.com/plugin-save-web-gimp-optimizar-imagen/#1_Instalacion_del_plugin_Save_for_web_en_GIMP
[9]: https://help.ubuntu.com/lts/serverguide/aptitude.html.en
[10]: http://www.nongnu.org/synaptic/
[11]: https://docs.gimp.org/en/gimp-tool-fuzzy-select.html
[12]: https://docs.gimp.org/en/gimp-tool-iscissors.html
[13]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/commit/c2f1299f473ec9d15fc2fbac8f04e4266e578d5b
[14]: /img/file-compress.ico
[15]: IMG-20180701-WA0004.jpg
[16]: IMG-20180701-WA0006.jpg
[17]: IMG-20180701-WA0009.jpg
[18]: IMG-20180701-WA0011.jpg
[19]: http://www.onlinewebfonts.com
[20]: rodeo-cheval.png
[21]: rodeo-moto.png
