---
title: "Fix Hugo Table of Contents"
subtitle: "Removal of wrapping HTML tag in JavaScript"
date: 2018-09-17T12:31:03+02:00
type: post
categories:
- blogging
tags:
- Hugo
- HTML
- JavaScript
toc: true
draft: false
---

### Background (TL;DR)

While setting up the _new_ version of [Staticman] for
[my demo GitLab pages][demo], I've read [developers' documentations][doc],
[setup guide][setup] and some _community_ blog posts so as to come up with
[my own guide][pp1].  It's originated and inspired from a variety of sources,
and refined according to hours of testing.  Consequently, despite the original
intention to keep things simple, I've finally come up with a post with over a
thousand words.

To pass my ideas in this post to visitors, it's better that they have an
_overview_ of the contents before actually looking into the details.  Therefore,
a table of contents is nice-to-have feature for this blog.

### Problem

[Hugo] provides a convenient function ``{{ .TableOfContents }}`` for this.  It
converts the headings of the page into an unordered list of anchors. However,
the post's title and subtitle, which are represented as `<h1>` and `<h2>`, are
taken into account.  As a result, the generated table of contents looks
_awkward_.

<ul><li><ul><li>
    <ul><li>Section 1
        <ul>
            <li>subsection 1.1</li>
            <li>subsection 1.2</li>
        </ul>
    </li></ul>
    <ul><li>Section 2</li></ul>
</li></ul></li></ul>

This explains [Hugo's issue&nbsp;#1778][hugo1778].

### Goal

To generate a table of contents like the one above.

### First attempt

In spite of the eleven likes for [Micheal Blum's snippet][mb] from
`layouts/partials/table-of-contents.html`, GitLab's CD informed me of
[the failure of job 98224023][build_fail] through email.

From the message, I opened this blog's [bash command list][pg1] to test the TOC.
Surprisingly, the generated links pointed to
[my post on laptop fan cleaning][pp2].

I tried looking at the related HTML template files, from which I _couldn't_
understand.  As I'm _not_ supposed to do testing testing after the term has
started, I abandoned this approach.

### Second attempt

Ryan Parman rewrote the aforementioned HTML template file and published it as
[gist&nbsp;a796d66f][a796d66f].  However, when I performed the _same_ test
again, the _same_ error was reproduced.

### Final solution

Thanks to [Yihui Xie's simple JavaScript][js], I managed to get this fixed, at
least, in runtime level.

#### Single page testing in browser

To understand what his script actually did, before including this in my theme's
footer, I ran some of the lines in the web developer's console in the browser.

- ✓: `toc = document.getElementById('TableOfContents')`
- ✗: `var toc = document.getElementById('TableOfContents')`

#### What does Xie's script do?

1. Extract the element with id `TableOfContents`.
2. Set variable `ul` to be the **u**nordered **l**ist directly under
`#TableOfContents`.
3. Proceed only when `ul` has more than one child.  (A TOC usually contains more
than one section, so only the top level `ul` containing one single item will get
"peeled off".)
4. Verify if `ul` exists, exit if it _doesn't_.
5. Set variable `li` to be the first child.  It's _supposed_ to be the first
**l**ist **i**tem.
6. Run a check if `li` really represents an `<li>`.
7. Extract the only list element's internal HTML use this to replace its
parent's external HTML.

Xie's script enabled me to move _one_ step forward toward the real solution.

<ul><li>
    <ul><li>Section 1
        <ul>
            <li>subsection 1.1</li>
            <li>subsection 1.2</li>
        </ul>
    </li></ul>
    <ul><li>Section 2</li></ul>
</li></ul>

The leftmost dot was _still_ there.  Nonetheless, the above verbal analysis of
his code allowed me to decouple the logic and to adapt it for other blog themes.

An _ugly_ hard code solution is to copy the above lines and rename the second
instance of the unordered list as `ul2` in the JavaScript---that's too hard to
swallow.  I would definitely go for a loop when doing repetitive tasks---that's
what machines are made for.

#### Adaptations to Xie's script to Beautiful Hugo

Since `ul` and `li` are changing elements during this tag unwrapping process, we
wrap the lines containing them with a loop.  As the first iteration is run
_unconditionally_, a do-while loop is chosen for this task.  Failing any of the
if statements would end the process, so we replace `return` with `break`.
Finally, just put _any_ condition that allows the loop to run.

{{< highlight js >}}
// Copyright (c) 2017 Yihui Xie & 2018 Vincent Tam under MIT

(function() {
  var toc = document.getElementById('TableOfContents');
  if (!toc) return;
  do {
    var li, ul = toc.querySelector('ul');
    if (ul.childElementCount !== 1) break;
    li = ul.firstElementChild;
    if (li.tagName !== 'LI') break;
    // remove <ul><li></li></ul> where only <ul> only contains one <li>
    ul.outerHTML = li.innerHTML;
  } while (toc.childElementCount >= 1);
})();
{{< /highlight >}}

#### Whole site regenerated

After local testing with `hugo server -D`, I published my changes to the site's
theme.  After updating the theme, the problem's now fixed.

To instantly view the changes on the site's theme, I have to modify the copy of
the theme's files under the local repo for the site.  However, it's better to
commit the changes to _another_ separate repo `~/beautifulhugo` so as to make my
theme _clean_.  It will be _laborious and prone to errors_ to type out the
_whole_ path by hand.  A more efficient solution will be posted in
[the next post][up1].

[Staticman]: https://staticman.net
[demo]: https://vincenttam.gitlab.io/test-hugo-staticman
[doc]: https://staticman.net/docs
[setup]: https://github.com/eduardoboucas/staticman/pull/219#issuecomment-417857360
[pp1]: https://vincenttam.gitlab.io/post/2018-09-16-staticman-powered-gitlab-pages/1/
[Hugo]: https://gohugo.io
[hugo1778]: https://github.com/gohugoio/hugo/issues/1778
[mb]: https://github.com/gohugoio/hugo/issues/1778#issuecomment-313895910
[build_fail]: https://gitlab.com/VincentTam/vincenttam.gitlab.io/-/jobs/98224023
[pg1]: /page/bash-commands/
[pp2]: /post/2018-07-04-fujitsu-lh532-fan-cleaning/
[a796d66f]: https://gist.github.com/skyzyx/a796d66f6a124f057f3374eff0b3f99a
[js]: https://github.com/rbind/yihui/blob/master/static/js/fix-toc.js
[up1]: /post/2018-09-17-copy-file-and-preserve-path/
