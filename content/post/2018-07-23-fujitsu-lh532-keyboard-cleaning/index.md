---
title: "Fujitsu LH532 Keyboard Cleaning"
subtitle: "Keyboard Cleaning DIY"
date: 2018-07-23T15:43:01+02:00
type: post
categories:
- technical support
tags:
- Fujitsu Lifebook
- LH532
- hardware
- keyboard
- cleaning
---

<i class="fa fa-exclamation-triangle" aria-hidden></i> 😖 <i class="fa fa-images"
aria-hidden></i> @ 📃! This post contains _unpleasant images_.  If you _don't_
wish to see them, please view this page with a _text browser_ or a _text mode
plugin_ of your browser.

### Rationale

#### Why?

My Fujitsu LH532 <i class="fa fa-laptop" aria-hidden></i> has been serving me
well as my production machine for almost six years.

The keyboard was quite *dirty*: it's full of hair and dandruff.  I had to clean
it for *personal hygiene*.

{{< gallery >}}
    {{< beautifulfigure src="20180709_043644.jpg" title="My dirty keyboard I" caption="Hair and dandruff under the keys" alt="dirty keyboard" >}}
    {{< beautifulfigure src="20180709_044103.jpg" title="My dirty keyboard II" caption="Hair and dandruff under the keys" alt="dirty keyboard" >}}
{{< /gallery >}}

#### Who?

Paying for IT technicians *isn't* cost effective.  A thorough keyboard cleaning
demands some skills and this takes several hours.  Consequently, the manpower
cost is going to surpass the cost of a new keyboard.  Apart from this, the
reasons for cleaning my keyboard *by myself* is three-pronged.

- ecological: avoid e-waste
- economical:
    + save money
    + If the IT technician accidentally broke a tiny part of your keyboard, they
    would have to compensate for your loss apart from getting *no* wages.  The
    risk of breaking things would outweight the profit.
- emotioinal: one gradually attaches emotions to something that (s)he has been
using for years.

### Detached keyboard from laptop

<i class="fa fa-exclamation-circle aria-hidden"></i> Always switch off
<i class="fa fa-laptop" aria-hidden></i> and remove <i class="fas
fa-battery-full" aria-hidden></i> before any repairing work <i class="fa
fa-wrench" aria-hidden></i>.

#### Goal

To separate <i class="far fa-keyboard" aria-hidden="true"></i> from <i
class="fa fa-laptop" aria-hidden></i> for cleaning.

{{< beautifulfigure src="20180709_011138_HDR.jpg" title="Fujitsu LH532 original keyboard" caption="detached keyboard from my laptop before cleaning" alt="Fujitsu LH532 keyboard" >}}

#### No screws holding keyboard in place

I tried prying out the keyboard from the top left-hand corner with my finest
flathead screw.

{{< beautifulfigure src="20180708_234331_HDR.jpg" title="Started keyboard detachment" caption="first step of keyboard detachment" alt="prying keyboard from top left-hand corner" >}}

Use another long thin flat tool to touch the clip (in the four holes above the
row of function keys).

However, having received so much resistance from the bottom right-hand corner, I
suspected that the keyboard had been screwed onto the laptop.  Therefore, I
*unscrewed the laptop's bottom lid*.  (This turns out to be *unnecessary*.)

{{< beautifulfigure src="20180709_001522_HDR.jpg" title="Unscrewing the bottom lid" caption="Top left: lid for fan and CPU maintenance; bottom left: lid for hard disk; middle: my precision screwdriver set; right: screws sorted according to sizes and positions" alt="prying keyboard from top left-hand corner" >}}

<i class="fa fa-exclamation-circle" aria-hidden></i> Some safety precautions
while operating with screwdrivers:

1. Always prepare some small containers to sort screws according to their sizes
and functions.
2. From [Geekerwan's video][1], one should install a part into the machine by
breaking the entire screwing process into "*multiple turns of slight
rotations*": rotate each screw slightly with the screwdriver each time, then
move to the next screw.
3. If your screwdriver's head *isn't* magnetic, use your dominant hand to hold
the screwdriver's handle, and use your remaining hand to hold the screw.

#### Disconnected keyboard from laptop

Due to the absence of reliable repair guides, I *didn't* dare to remove the lid.
Instead, I resumed the process of detaching the keyboard.  I *started with
slight forces*, and *gently increased the force* until that it's enough to
detach the keyboard.

<i class="fa fa-exclamation-circle" aria-hidden></i> After that, the *keyboard
remained connected to the laptop with a cord*.  The cord was *hidden*
underneath: it *couldn't* be seen while one was prying the keyboard out.  That's
why the keyboard deserved to be treated *gently*.

{{< beautifulfigure src="20180709_000517_HDR.jpg" title="Detaching keyboard" caption="This allows cleaning the dandruff stuck inside the long thin cavity." alt="Fujitsu LH532 keyboard half detached" >}}

At this stage, I realised [my wrong guess][2]: *no* screw was used to fix the
keyboard onto the laptop.  Since the keyboard was attached to the laptop by the
cord *only*, it's better to disconnect the cord first.

{{< beautifulfigure src="20180709_001359_HDR.jpg" title="Keyboard almost detached" caption="It reamined to disconnect the cord." alt="Fujitsu LH532 keyboard almost detached" >}}

In some demo videos, the slot released itself with a slight push at one of the
ends of the slot using a screwdriver.  Nonetheless, I *didn't* manage to apply
this skill to my laptop.  Instead, I opened the slot from the middle with my
finger.  I was careful enough *not* to touch any surrounding sensitive parts.

{{< beautifulfigure src="20180709_002021_HDR.jpg" title="Keyboard cord disconnected"  caption="I didn't know how to use a tiny flathead screwdriver for that." alt="Fujisu LH532 keyboard slot" >}}

#### Screwed the lid back

{{< beautifulfigure src="20180709_003914_HDR.jpg" title="Laptop bottom lid restoration" caption="I used a crosshead screwdriver for that." alt="Fujisu LH532 bottom lid screwed" >}}

### Key removal and cleaning

<i class="fa fa-exclamation-circle" aria-hidden></i> When a tiny part falls on
the ground, pick it up *immediately* to avoid accidental damages or loss.

#### Key removal

<i class="fa fa-exclamation-circle" aria-hidden></i> To avoid any damage on the
silicon cap under each key, *don't* insert the screwdrivers too deep into the
cavity under each key.

- Normal size keys were relatively easy to remove.
    1. Place one screwdriver on the top side and another screwdriver at the
    bottom on the right side.
    2. Pry them up *gently*.  The key should pop out.  They sometimes jumped far
    away so picking them up is a bit tedious.
- Small keys required more care.
    1. Start prying from either one of the longer sides.
    2. Idem for another longer side.
- Bottom row keys (except space bar): similar to normal size keys
- Remaining longer keys: they came with metal bars, which evenly distribute
pressure to the entire key, so watching some demo videos can help, even though
the work had been done on other models.  Except the space bar, the metal bar
underneath is U-shaped.  As a result, start from the top side before the bottom 
ide.

#### Hinge removal

- Hinges for normal size keys were relatively easy to remove.

    {{< beautifulfigure src="20180709_020415_HDR.jpg" title="Normal size hinge removal" caption="Gently push the two top corners" alt="Normal size hinge removal" >}}

- Hinges for small keys required more care.

    {{< beautifulfigure src="20180709_024052_HDR.jpg" title="Small size hinge removal" caption="Pause break's hinge's colour is different from delete's hinge." alt="Small size hinge removal" >}}

    At first, I *didn't* know how to handle them.  Already worked for two hours,
    I felt tired and pried F7 key hard.

    {{< beautifulfigure src="20180709_024551.jpg" title="F7 key removal" caption="F7's hinge was bent." alt="F7 key hinge bent" >}}

    What's worse, I *broke* F5 key's hinge.

    {{< beautifulfigure src="20180709_035007.jpg" title="F5 hinge broken" caption="I broke it after pushing and prying from the bottom right corner" alt="F5 key hinge broken" >}}

    Having experimented on several hinges for small keys, I finally found a
    proper way to remove it.

    {{< beautifulfigure src="20180709_035419.jpg" title="Small key's hinge removal" caption="Just like cooking, it's about feeling." alt="small key hinge removal" >}}

    - Observations
        + Prying from the left-hand side was *impossible*: the metal hook on the
        left-hand side held one component of the hinge.
        + Prying from the right-hand side was also *impossible*: the metal hooks
        on the right-hand side held another component of the hinge.
        + The middle part is *elastic* and *flexible*.
    - Steps
        1. Lift up the hinge with a flathead screwdriver.
        2. Place another screwdriver adjacent to either one of the two joints to
        keep the hinge lifted up.
        3. Slightly twist the screwdriver in step 2 to release the joint.
        4. Repeat steps 2--3 for another joint.
        5. With the joints *detached*, the two components of the hinge could be
        easily removed.

#### Keyboard cleaning

*Without* compressed air, I tried my best to get rid of the dandruff by
brushing the keyboard with an old toothbrush and by turning the keyboard upside
down.  I removed the hair by hand.  It took me one and a half hour to take away
*most* of them.

{{< beautifulfigure src="20180709_043644.jpg" title="Difficult hair removal" caption="Much hair was stuck under the silicon cap" alt="hair stuck at keyboard" >}}

{{< beautifulfigure src="20180709_044103.jpg" title="F12's silicon cap detached" caption="I unluckily detached F12's silicon cap" alt="F12 silicon cap detached" >}}

#### Cleaned the keys and hinges

<i class="fa fa-exclamation-circle" aria-hidden></i> Some safety precautions:

1. Prepare
    - a plastic basin to hold the solution
    - a container to hold the keys and hinges
    - a towel to dry the detachable parts
2. Always put your parts into/onto one of these three tools to avoid losing them.

I *immersed all keys and all hinges into a cleaning powder solution* for another
one and a half hour in order to *remove the grease* on the surfaces of the keys
and to *kill the microorganisms* on the hinges.  Then I transferred these
removable parts onto one towel before replacing the solution with water, which
rinsed off the remaining solution on them.  They were eventually dried on
another towel.

{{< beautifulfigure src="20180709_062923.jpg" title="Key cleaning solution" caption="Some keys had been washed and they were being dried." alt="Key cleaning solution" >}}

### After cleaning

*Unaware* of the mechanism of the keyboard, I had detached *five* silicon caps.

{{< beautifulfigure src="20180709_151520.jpg" title="Detached silicon caps after cleaning" caption="I had detached them by accident during key removal and cleaning." alt="five detached silicon caps" >}}

#### Stuck detached silicon caps back

<i class="fa fa-exclamation-circle" aria-hidden></i> Some safety precautions
while operating with super glue:

1. Work in a *well-ventilated* area to dilute the smell of super glue.
2. *Wear a pair of gloves* to avoid super glue from sticking your hands.
3. *Close the lid of the super glue* when it *isn't* in use to prevent the super
glue from drying.
4. Squeeze a little super glue onto a sheet of paper.  *Gently* dip the surface
which you want to stick onto the super glue.  Bear in mind that *less is more*.
5. Stick the surface in step 4 onto another one.  Try to get the position right
with one stroke.

I *applied super glue to stuck each of them to the dotted circle*.  After
overnight working, I was so exhausted that I added too much super glue to the
buttons N and &larr;.  As a result, these two keys *didn't* work well when I
booted into [GRUB][3]'s CLI.

#### Some stuck caps suck

I detached the silicon cap for these two keys again.  Since they had been stuck
to the keyboard with super glue, I used a flathead screwdriver to *gently*
remove them.  This was *suboptimal*, but I thought that's better than passing
any organic solvent on the surface of the keyboard.

{{< beautifulfigure src="20180709_185543.jpg" title="← key fix failure" caption="Excess super glue under the silicon cap." alt="five detached silicon caps" >}}

The layer of super glue between the silicon cap and the keyboard sensor stopped
the later from detecting pressure on the key.  To restore the function of the
buttons N and &larr;, I removed that layer of super glue.

{{< beautifulfigure src="20180709_192132.jpg" title="Removed super glue layer" caption="Super glue removed from the silicon cap for buttons N and ←" alt="super glue layer removed" >}}

Finally the N key was fixed: *no* noticeable difference from other normal size
keys could be observed, but my fix for the &larr; key was *suboptimal*: this key
would still respond to a press, but its *silicon cap lost part of its
elasticity*.

### An outing in _Le Marais_ district

Two days later, I walked on the streets in _Le Marais_ situated in the fourth
arrondissement at Paris to take a break from my technical work.  I passed
through _[le Temple du Marais][4]_.

{{< beautifulfigure src="marais-small.jpg" link="20180711_215200.jpg" title="Le Temple du Marais" caption="\"Eglise verte\" means \"green church\"." alt="Le Temple du Marais" >}}

I was *surprised that my church was going green*.  Renewing an old keyboard is
certainly a way to *do what they preach*.

You may see original photos (with higher resolution licensed under CC-BY) in
[my Flickr album][5].

[1]: https://youtu.be/1sFuS2y1B1o
[2]: #no-screws-holding-keyboard-in-place
[3]: https://www.gnu.org/software/grub/
[4]: http://temple.dumarais.fr/
[5]: https://flic.kr/s/aHsmptxYUt
