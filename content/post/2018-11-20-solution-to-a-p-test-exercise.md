---
title: "Solution to a $p$-test Exercise"
date: 2018-11-20T12:09:46+01:00
type: post
categories:
- math
tags:
- calculus
- Math.SE
draft: false
markup: mmark
---

I intended to answer [Maddle's $p$-test question][1], but T. Bongers has beaten
me by two minutes, so I posted my answer here to save my work.

The problem statement

> This is the sum:
> $$\sum\limits_{n=3}^\infty\frac{1}{n\cdot\ln(n)\cdot\ln(\ln(n))^p}$$
> How do I tell which values of $p$ allow this to converge? The ratio test isn't
> working out for me at all.

### Unpublished solution

The integral test will do.

$$
\begin{aligned}
& \int_3^{+\infty} \frac{1}{x\cdot\ln(x)\cdot\ln(\ln(x))^p} \,dx \\
&= \int_3^{+\infty} \frac{1}{\ln(x)\cdot\ln(\ln(x))^p} \,d(\ln x) \\
&= \int_3^{+\infty} \frac{1}{\ln(\ln(x))^p} \,d(\ln(\ln(x))) \\
&= \begin{cases}
[\ln(\ln(\ln(x)))]_3^{+\infty} & \text{if } p = 1 \\
\left[\dfrac{[\ln(\ln(x))}{p+1}]^{p+1} \right]_3^{+\infty} & \text{if } p \ne 1
\end{cases}
\end{aligned}
$$

When $p \ge 1$, the improper integral diverges.  When $p < 1$, it converges.

[1]: https://math.stackexchange.com/q/3005870/290189
