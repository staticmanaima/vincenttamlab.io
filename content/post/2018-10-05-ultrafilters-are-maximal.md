---
title: "Ultrafilters Are Maximal"
date: 2018-10-05T13:19:36+02:00
type: post
categories:
- math
tags:
- topology
draft: false
markup: mmark
---

Ultra filter
: A filer $\mathcal{F}$ containing either $Y$ or $Y^\complement$ for _any_
$Y \subseteq X$.

Two days ago, I spent an afternoon to understand Dudley's proof of this little
result.

> A filter is contained in some ultrafilter.  A filter is an ultrafilter iff
> it's maximal.

At the first glance, I _didn't_ even understand the _organisation_ of the proof!
I'm going to rephrase it for future reference.

- only if: let $\mathcal{F}$ be an ultrafilter contained in another filter
$\mathcal{G}$.  If $\mathcal{F}$ _isn't_ maximal, let $Y \in \mathcal{G}
\setminus \mathcal{F}$.  Since $\mathcal{F}$ is an ultrafilter, either $Y \in
\mathcal{F}$ or $Y^\complement \in \mathcal{F}$.  By construction of $Y$, only
the later option is possible, so $Y^\complement \in \mathcal{G}$ by hypothesis,
but this contradicts our assumption $Y \in \mathcal{G}$: $\varnothing = Y \cap
Y^\complement \in \mathcal{G}$, which is _false_ since $\mathcal{G}$ is a
filter.
- if: let $Y \subseteq X$.  "Extend" $\mathcal{F}_{\max}$ to $\mathcal{G}$ so
that $Y \in \mathcal{G}\_1$ or $Y^\complement \in \mathcal{G}\_2$.  Apply
maximality so that $\mathcal{G}\_i = \mathcal{F}\_{\max}$ for each $i \in
\lbrace 1,2 \rbrace$.  To construct such $\mathcal{G}$, make a key observation:
either one of the following is true.

  1. $\forall G \in \mathcal{F}_{\max}, G \cap Y \ne \varnothing$
  2. $\forall F \in \mathcal{F}_{\max}, F \cap Y^\complement \ne \varnothing$

    To understand this, suppose $(1)$ is _false_.  There exists $G \in
    \mathcal{F}\_{\max}$ such that $G \cap Y = \varnothing$.  (i.e. $G \subseteq
    Y^\complement$)  For any $F \in \mathcal{F}\_{\max}$, $F \cap G \ne
    \varnothing$ since $\mathcal{F}\_{\max}$ is a filter.  Take intersection
    with $F$ in the first set inclusion to obtain $F \cap G \subseteq F \cap
    Y^\complement$, so $F \cap Y^\complement \ne \varnothing$.  We have just
    proved $(2)$.

    <i class="fas fa-info fa-lg fa-pull-left" aria-hidden></i>
    To visualise this, take, for example, the filter to be the collection of
    open neighbourhoods of any point in $Y^\complement$.

- Since $\mathcal{F}$ can be uncountably infinite, Zorn's lemma is needed.  We
apply this on the collection $\mathcal{X}$ of filters in $X$.  For any chain
$\mathcal{C}$ of filters in $X$, check that $\mathcal{U} = \cup \mathcal{C}$ is
a filter by definition.  Then invoke the lemma to get a maximal element
$\mathcal{F}_{\max}$ and we're done.
