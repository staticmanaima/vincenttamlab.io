---
title: "Brightness Key on Linux"
subtitle: "Adjust brightness on Xubuntu 18.04"
type: post
categories:
- technical support
tags:
- Linux
- brightness
date: 2018-07-09T23:21:17+02:00
---

### Problem

One can use [xrandr to adjust screen brightness][1].  However, the
factor (default to 1.0) is *relative* to the current brightness.
**How to enable the brightness adjust keys <i class="far fa-sun"
aria-hidden></i><i class="fa fa-caret-up" aria-hidden></i>/<i
class="far fa-sun" aria-hidden></i><i class="fa fa-caret-down"
aria-hidden></i> on the keyboard under Xubuntu 18.04?**

### Solution

[Toz's post][2] on Ubuntu Forum works like a charm.  The default value
for `GRUB_CMDLINE_LINUX` is an *empty* string.  Changing it to
`acpi_backlight=vendor` and updating the [GRUB][3] has solved the
problem since the next boot.

### Remarks

Unfortunately, the thread is now *closed*, so I *can't* leave a
comment to remind myself of this useful post.  Therefore, I've written
this post.

[1]: http://askubuntu.com/questions/62249/ddg#62270
[2]: https://ubuntuforums.org/showthread.php?t=2080301
[3]: https://www.gnu.org/software/grub/
