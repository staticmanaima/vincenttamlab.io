---
title: "Zsh and NPM Setup on Ubuntu"
subtitle: "Automate settings with Oh My Zsh"
date: 2018-09-06T13:00:53+02:00
type: post
categories:
- technical support
tags:
- Zsh
- NPM
draft: false
---

### Problem

After installing NPM throught Ubuntu's default `apt-get` manager, I got
[permission errors][so-npm].  Though `sudo` can solve the problem, that's _not_
the right way in principle because NPM is supposed to hold different versions of
Node.js packages for testing.  It's _insecure_ to execute `npm` with `sudo`
priviledges.

### Solutions

1. Install Zsh to improve efficiency
2. Grab [Oh My Zsh][ohmyzsh] to instal NVM as its plugin.

        sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

3. Use NVM (NPM Version Manager) to manage NPM.
4. Use NPM to manage Node.js.

### External links

1. [Linux hint][lh]
2. [Sven Boekhoff's Zsh install and setup guide][sb]

[so-npm]: https://stackoverflow.com/q/16151018
[ohmyzsh]: https://github.com/robbyrussell/oh-my-zsh
[lh]: https://linuxhint.com/install_zsh_shell_ubuntu_1804/
[sb]: http://www.boekhoff.info/how-to-install-nodejs-and-npm-using-nvm-and-zsh-nvm/
