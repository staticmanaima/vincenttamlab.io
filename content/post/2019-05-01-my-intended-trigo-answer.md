---
title: "My Intended Trigo Answer"
date: 2019-05-01T22:24:45+02:00
categories:
- math
tags:
- trigonometry
- Math.SE
draft: false
markup: mmark
---

The Math.SE question _[$2\cos(2x) - 2\sin(x) = 0$][1]_ has attracted several
answers from high-rep users.

> I am expanding @rhombic's comment into an answer.
>
> $$
> \begin{aligned}
> 2\cos(2x)-2\sin(x)&=0 \\
> 2 - 4\sin^2(x)-2\sin(x)&=0 \\
> 2\sin^2(x)+\sin(x) - 1&=0 \\
> (2 \sin(x) - 1)(\sin(x) +1) &= 0 \\
> \sin(x) = \frac12 \text{ or } \sin(x) &= -1 \\
> x = \frac{\pi}{6}, \frac{5\pi}{6} \text{ (rejected) or } & \frac{3\pi}{2}
> \text{ (rejected)}
> \end{aligned}
> $$

[1]: https://math.stackexchange.com/q/3210133/290189
