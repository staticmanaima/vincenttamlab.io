---
title: "Weak LLN Practice"
date: 2018-12-02T08:11:56+01:00
type: post
categories:
- math
tags:
- probability
- Math.SE
draft: false
markup: mmark
---

My intended answer to [a weak LLN problem on Math.SE][pblm].

> **Problem**: Suppose $(X_n)$ is a sequence of r.v's satisfying $P(X_n=\pm\ln
(n))=\frac{1}{2}$ for each $n=1,2\dots$. I am trying to show that $(X_n)$
satisfies the weak LLN.

> The idea is to show that $P(\overline{X_n}>\varepsilon)$ tends to 0, but I am
unsure how to do so.

> **My solution**: As in the accepted answer in OP's previous question
https://math.stackexchange.com/q/3021650/290189, I'll assume the independence of
$(X_n)$.  By Chebylshev's inequality,

> $$
> P(|\bar{X}_n|>\epsilon) = P(|\sum_{k=1}^n X_k|> n\epsilon) \le
> \frac{\sum_{k=1}^n var(X_k)}{n^2 \epsilon^2} = \frac{\sum_{k=1}^n
> (\ln(k))^2}{n^2 \epsilon^2} \le \frac{(\ln(n))^2}{n \epsilon} \to 0
> $$

Recall: $(\ln(n))^p/n \to 0$ whenever $p \ge 1$.  To see this, make a change of
variables $n = e^x$, so that it becomes $x^p / e^x$.

[pblm]: https://math.stackexchange.com/q/3022328/290189
