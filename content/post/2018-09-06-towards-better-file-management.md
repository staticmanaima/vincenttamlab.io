---
title: "Towards Better File Management"
subtitle: "Use git-annex like your local library"
date: 2018-09-06T11:05:53+02:00
type: post
categories:
- technical support
tags:
- git-annex
draft: false
---

{{< beautifulfigure src="/post/2018-07-23-fujitsu-lh532-keyboard-cleaning/marais-small.jpg" link="/post/2018-07-23-fujitsu-lh532-keyboard-cleaning/20180711_215200.jpg" title="Le Temple du Marais" caption="\"Eglise verte\" means \"green church\"." alt="Le Temple du Marais" >}}

### Background: 🈶 ⛪ → 🌿

<i class="far fa-envelope" aria-hidden></i> → 📧 In the recent decade,
organizations are replacing courrier with email.  The reason is two-folded.  🌿
First, the later is more environmentally-friendly.  📨 Second, the later delivers
the content to the recipient quick as a flash.

📧 📎 <i class="fas fa-file-signature" aria-hidden></i> When one sends an email,
one sometimes wants to attach a file to an email.  If one has to sign a paper
document and send it by email, then one will probably need a scanner.  (Despite
the emergence of scanning apps on mobile devices, the quality of a scanner is
normally better since the page is flattened during the scan.)

### Problem 📄 💾 → 🖥 / ☁

The documents are often saved to _some local/cloud storages_.  How
to manage their versions?

### Solution Primer

Add a date to file name.

However, when one has hundreds of _unorganized_ files in a folder, how can one
deal with `foo-bar-2018.odt` and `2018-foo-bar.odt`?

### Use `rsync`

`rsync` is a quick solution, but it _doesn't_ scale.  One has to do the backup
for _each_ machine.  That's no big deal for several devices, but that's _not_
the way for sharing files among an _array_ of computers.

### Use git-annex

[git-annex] is like a librarian.  Only symbolic links 🔗 are written into the <i
class="fab fa-git" aria-hidden></i> index.  The binary files are stored as
binary objects under 📁 `.git/annex`.

### <i class="fas fa-external-link-alt" aria-hidden></i> External links

1. [git-annex's offical page][git-annex]
2. [📄 🔁 🖥 Sync ses ordi][phy]

[git-annex]: https://git-annex.branchable.com/
[phy]: https://phyks.me/2014/08/synchroniser-ses-ordinateurs-12.html
