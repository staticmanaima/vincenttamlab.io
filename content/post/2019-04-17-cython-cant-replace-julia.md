---
title: "Cython Can't Replace Julia"
date: 2019-04-17T16:57:32+02:00
categories:
- technical support
tags:
- math
- Julia
- Cython
draft: false
---

This is a linklog to
[Christopher Rackauckas's article about Julia, Cython & Numba][1].  Unluckily, I
*don't* have time to read this now.  Hope I can return to this later.

[1]: http://www.stochasticlifestyle.com/why-numba-and-cython-are-not-substitutes-for-julia/
